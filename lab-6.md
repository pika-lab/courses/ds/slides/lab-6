class: center, middle

# T-6 -- All we need is ReST

.middle.center[[__Giovanni Ciatto__](mailto:giovanni.ciatto@unibo.it)]

.middle.center[and]

.middle.center[[Andrea Omicini](mailto:andrea.omicini@unibo.it)]

---

## Motivation

- __Web services__ are a common pattern for nowadays distributed systems
    * Probably, most applications you use every day are web services
    * e.g. Facebook, Instagram, YouTube, PayPal, Spotify, Netflix, and, yaeh, __Trenitalia__ too

<!-- pause -->

- Most web services adhere to the _**Re**presentational **S**tate **T**ransfer_ (ReST) architectural style, which is the _de facto_ standard for web services

<!-- pause -->

- This means they expose a **ReSTfull API** which is used to wrap a server implemented with some arbitrary technology
    * Such an API is usally formalised by means of some __specification language__ and it's publicly available
    * e.g. [YouTube Data API v3](https://developers.google.com/apis-explorer/#p/youtube/v3/)

<!-- pause -->

- Clients -- possibly implemented with some different technology -- can interact with the web service simply by means of such an API

<!-- pause -->

- The ReST principles are often declinated into a number of good practices aimed at creating __scalable__ and __interoperable__ systems which are easy to design, develop, and maintain

---

## Lecture goals

- Recall the Hyper Text Transfer Protocol (__HTTP__)
    * (Even if it is not the only choice, HTTP is usually the base protocol of choice)

<!-- pause -->

- Understand how ReST principles are reifyied into good __practices and patterns__ for HTTP-based application

<!-- pause -->

- Learn how to produce a (semi-)formal __API specification__ by means of _de facto_ standard tools

<!-- pause -->

- Learn how a ReSTful __server__ should be designed and implemented 

<!-- pause -->

- Understand how __authentication and authorization__ are performed over the network

---

## Recall the HTTP Protocol

The Hyper Text Transfer Protocol (HTTP) essentially states:

<!-- pause -->

* one or more __clients__ can communicate with a __server__ by means of _synchronous_ RPC calls

<!-- pause -->

* each client issues a __request__ and waits for a __response__ from the server. In any case:
    + requests are directed towars an __URL__, i.e. an name for a __resource__ hosted by the server
    + requests specify a __method__, i.e. an operation with a standard semantics to be performed on the __resource__
    + responses carry a __status code__ with a standard semantics 

<!-- pause -->

* each server is indefinitely __waits__ for incoming requests, and, as soon as they arive, it produces and sends back a response
    + in doing, the server may interact with other server or __databases__, behaving as a client w.r.t. them

<!-- pause -->

* requests may possibly provide some __input arguments__, possibly carrying some __return value__

---

layout: true

### Messages, methods, URLs, headers, and status codes

---

#### HTTP Messages: the request structure

.midde.fullWidth[![The HTTP __request__ message](./http_request.gif)]

---

#### HTTP Messages: the response structure

.midde.fullWidth[![The HTTP __response__ message](./http_response.gif)]

---

#### Methods and CRUD operations

.middle.fullWidth[
    ![The HTTP methods](./http_methods.png)
]

<!-- pause -->

- CRUD = `C`reate, `R`read, `U`pdate, `D`elete

<!-- pause -->

- Other HTTP methods exist (e.g. `TRACE`, `HEAD`, `CONNECT`, or `OPTIONS`): more details are provided [here](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html)

---

#### The URL structure

.threeQuartersWidth.center.middle[
    ![The URL structure](./http_url.png)
]

---

#### Path and query parameters

- Paths are usually in the form:
```
/ collectionOfResources / {resourceID} / subcollection / :subresourceID
```
_(without spaces)_ where the notations `{pathParam1}` and `:pathParam2` denote __path parameters__

<!-- pause -->

- Query strings are usually sequences of **key-value pairs** in the form:
```
? key1 = value1 & key2 = value 2 & ...
```
_(without spaces)_, where `?` denotes the beginning of the query string, `&` is the key-value pair separator, and `=` separates keys from values

---

#### Headers

- HTTP headers allow the client and the server to pass additional information with the request or the response

<!-- pause -->

- The general structure of a HTTP header is a colon-separated pairs:
    + __`StandardHeader`__`: Value` for standard HTTP headers
    + __`X-CustomHeader`__`: Value` for custom (non-standard) headers 
        * (notice the `X-` prefix)

<!-- pause -->

- We will only focus on the following headers: 
    + __`Authorization`__: usually present in requests, contains the user credentials, arbitrary encoding
    + __`Content-Type`__: usually present in both requests and responses, contains the __`MIME`-type__ of the message body
    + __`Accept`__: usually present in requests, contains `MIME`-type(s) of the client is able to understand
        * The server is then expected to provide a response body whose `MIME`-type is in the `Accept`-list of the request

---

#### Standard MIME Types

- The __Multipurpose Internet Mail Extensions__ (MIME) type is a standardized way to indicate the nature and format of a document
    * general structure __`type`__`/`__`subtype`__

<!-- pause -->

.center.ninetyPercentWidth[![MIME types](./mime_types.png)]

---

#### HTTP message bodies

- Message bodies carry the __representations__ of the resources being manipulated by the client through the server

<!-- pause -->

- They usually consist of __documents__ (i.e. files) adhering to some particular __schema__ (i.e. data type)

<!-- pause -->

- Such documents are usually __encoded__ according to a MIME type
    + Most common ones are `text/`__`html`__, `application/`__`xml`__, `application/`__`json`__, and [`application/`__`yaml`__](https://en.wikipedia.org/wiki/YAML)

<!-- pause -->

- Schemas are usually defined by means of __schema languages__:
    + [XML Schema](https://www.w3schools.com/xml/schema_intro.asp) for XML
    + [JSON Schema](https://json-schema.org/learn/getting-started-step-by-step.html) for JSON or YAML
    + Some attempts exist to create a (more) target-neutral schema language: [OpenAPI specification](https://swagger.io/specification/#schemaObject) 

---

#### Example: OpenAPI schema for resources of type `User`

.middle[
```yaml
User:
    type: object
    properties:
        id:
            type: string
            format: uuid
        username:
            type: string
        fullName:
            type: string
        email:
            type: string
            format: email
        password:
            type: string
            format: password
        link:
            type: string
            format: url
        role:
            type: string
            enum: [admin, user]
```
]

---

#### Example: a JSON representation of the User

.middle[
```json
{
    "id": "445e76e5-1c37-498d-8d1f-42c15ce8645f",
    "username": "gciatto",
    "fullName": "Giovanni Ciatto",
    "email": "giovanni.ciatto@unibo.it",
    "password": "p4ssw0rd",
    "link": "www.example.com/my-app/v1/users/gciatto",
    "role": "admin"
}
```
]

#### Example: a YAML representation of the User

.middle[
```yaml
id: 445e76e5-1c37-498d-8d1f-42c15ce8645f
username: gciatto
fullName: Giovanni Ciatto
email: giovanni.ciatto@unibo.it
password: p4ssw0rd
link: www.example.com/my-app/v1/users/gciatto
role: admin
```
]

---

#### HTTP status codes

.middle.center.fullWidth[![The HTTP status codes](./http_status_codes.png)]

---

#### HTTP status code ranges

* `1xx` (Informational): The request was received, continuing process
* `2xx` (Success): The request was successfully received, and accepted
    - __`200` OK__: Standard response for successful HTTP requests
    - __`204` No content__: Like ok but response contains no body
* `3xx` (Redirection): Further action are needed to complete the request
* `4xx` (Client Error): The request contains bad syntax or cannot be fulfilled
    - __`400` Bad Request__: The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax)
    - __`401` Unauthorized__: Authentication is required and has failed or has not yet been provided
    - __`403` Forbidden__: Authentication was successful but the client has no permission to perform the requested operation
    - __`409` Conflict__: Indicates that the request could not be processed because of conflict in the current state of the resource
* `5xx` (Server Error): The server failed to fulfill an apparently valid request
    - __`500` Internal Server Error__: A generic error message
    - __`501` Not Implemented__: The server either does not recognize the request method, or it lacks the ability to fulfil the request
---

layout: false

## ReST project design workflow

When designing a ReST-full web-service, you are encouraged to follow this procedure: 

<!-- pause -->

1. structure your application in terms of **resources** and **sub-resources**

<!-- pause -->

0. for each (sub-)resource, define a possibly parametric **path**

<!-- pause -->

0. for each path, define the **methods** it supports, thus defining a **route**

<!-- pause -->

0. for each route, define:
    - the supported __query parameters__ for the request
    - the supported input and output __MIME types__
    - the supported __schema__ for the request body 
    - all the possible results, i.e.:
        + all possible __status codes__...
        + ... and the corresponding schemas for the response bodies
    - optionally, you may also define if __authentication__ is required and which sorts of __authorizations__ are needed

<!-- pause -->

*Essentially, this procedure is aimed at defining the service __(API)__, i.e. its public interface*

---

### OpenAPI Specification (OAS) and Swagger
The design and test phases of a ReST-full web service __API__ can be greatly simplified by some (almost) standard tachnologies:

<!-- pause -->

- The [**OpenAPI Specification**](https://swagger.io/specification/v2/) (OAS) is a formal language for describing the __API__ of the service
    * Current version of the specification is [version 3](https://swagger.io/specification/), but we will use [version 2](https://swagger.io/specification/v2/) since it's more mature 
    * Technically, one may actually define a service API by writing a __YAML/JSON file__ adhering to the OAS

<!-- pause -->

- [**Swagger**](https://swagger.io/) is a community developing and maintaining the OAS, other than a number of useful tools easing the life of API designers and implementors
    * e.g. [__Swagger Editor__](https://editor.swagger.io/) is a web-based editor for writing specification files
    * e.g. [__Swagger Codegen__](https://swagger.io/tools/swagger-codegen/) is a tool generating client and server stubs out of an OAS specification file

<!-- pause -->

- [**Swagger Hub**](https://app.swaggerhub.com) is a web repository letting people edit, store, and publish their services APIs

<!-- pause -->

_Consider reading the [**OAS definition**](https://swagger.io/specification/v2/) while producing/inspecting some OAS specification file_

---

### Main structure of a OAS specification file

```yaml
swagger: "2.0" # OAS version
info:
  # title, version, description, authors info
schemes:
  # list of supported protocols, e.g. http, https, ws, wss
securityDefinitions:
  # supported authentication/authorization schemas
paths:
  # supported paths
  /collection/{resourceID}:
    # supported routes for this path
    parameters:
      # list of common parameters for all routes in this path (e.g. "resourceID")
    get:
      # supported IO values for the route "GET /collection/{resourceID}"
      parameters:
        # list of query and body parameters for this route
      responses:
        # possible status codes and their respective outcomes
    post:
      # supported IO values for route "POST /collection/{resourceID}"
definitions:
  # data types definitions (i.e., schemas)

# other fields
  
```

---

### Route definition according to OAS

```yaml
paths:
  /users: # the collection resource containing users
    get: # the GET method returning a representation of such collection
      parameters:
        - in: "query"
          name: "skip" # a query param limiting the amount of returned users
          type: "integer"
        - in: "query"
          name: "limit" # a query param limiting the amount of returned users
          type: "integer"
      produces: # supported output MIME types
        ["application/json", "application/xml", "application/yaml"]
      responses:
        200: # in case the request can be served...
          description: "Success"
          schema:
            type: "array" # ... an array of users is returned
            items:
              # reference to the User definition defined below
              $ref: "#/definitions/User"
        # Failure because of missing credentials or lack of access rights
        401: { description: "Unauthorized." }
        403: { description: "Forbidden." }
definitions:
  User: # the user schema defined a few slides ago
```

---

## Exercises 6-*

This week you should accomplish the following exercises:

<!-- pause -->

* __Exercise 6-0__: Becoming confident with the OAS syntax, Swagger Editor, and Swagger Hub

<!-- pause -->

* __Exercise 6-1__: Editing an existing OAS specification file

<!-- pause -->

* __Exercise 6-2__: Implementing a web-service adhering to a given OAS specification file

<!-- pause -->

_All exercises will be based on the same running example: the **Web-Chat**_

<!-- pause -->

_**Optional**: tests for exercise 6-2 are not enough_

* you can propose some, I will integrate them into the exercise

* for each __triplet__ of _novel & useful_ test posted on the forum, you'll earn a badge

---

layout: true

### The Web-Chat informal specification

---

- The Web-Chat application consists of an HTTP service functioning a central hub for multiple clients chatting by means of __chat rooms__

<!-- pause -->

- Clients can register themselves using unique usernames, addresses, and passwords
    + Upon registration, they are endowed with an [Universally unique identifier](https://it.wikipedia.org/wiki/Universally_unique_identifier) (UUID)
    + Clients can either be `admin`s or `user`s
    + Unlogged clients can chat too, but they have no explicit identity or role
    + A logged client can be referenced by means of its username, its address, or its UUID

<!-- pause -->

- Clients can create chat rooms -- thus becoming their __owners__ --, whereas other clients may __join__ -- thus becoming __members__ --, or leave such chat rooms
    + Chat rooms can be referenced by means of their name, which is assumed to be unique
    + The members of a chat room can see the whole sequence of messages published within that chat room or just a sub-sequence
    + The members of a chat room can see the whole set of members of the chat room

---

- Owners can create their chat rooms with three different access levels:
    + __public__ chat rooms can be read and written by anyone, there including unlogged users: membership is unimportant here
    + __open__ chat rooms can be read and written only by their members, but any logged user can join them
    + conversely, in __private__ chat rooms only the owner can assign memberships 

<!-- pause -->

- `admin`s can inspect and manage the list of registered users
    - a registered `user` can be removed either by an `admin` or by him/her self

<!-- pause -->

- The list of currently existing chat rooms is publicly available

<!-- pause -->

- _More info are available on the OAS specification for the Web-Chat App, available [here](https://app.swaggerhub.com/apis/PIKA-lab/Web-Chat/1.0.0)_
---

layout: false

### Authentication and Authorization for Lab-6

In this Lab, we will consider a _very trivial and **insecure**_ authentication and authorization schema:

<!-- pause -->

* Upon registration, clients are assumed to provide an identifier and a password (i.e., their __credentials__), which are stored on the server side
    + the server should prevent the same identifier from being registered twice
    + clients are assumed to be registered as `user`s
    + `admin`s are __hardcoded__ into the provided implementation stub 

<!-- pause -->

* When performing an HTTP request, clients are assumed to provide their credentials as a __JSON string__ contained within the __`Authorization`__ header of the request. 
The JSON string, should have one of the following forms: 
    + `{ `__`"id"`__`: "uuid here", `__`"password"`__`: "password here" }`
    + or `{ `__`"username"`__`: "username here", `__`"password"`__`: "password here" }`
    + or `{ `__`"email"`__`: "email here", `__`"password"`__`: "password here" }`

<!-- pause -->

* When performing an HTTP request, clients are considered __authenticated__ if they credentials match the ones which are stored on the server side

<!-- pause -->

* When performing an HTTP request, clients are considered __authorized__ if they are authenticated _and_ they their role is enabled to perform the requested operation

---

### Exercise 6-0: Become confident with Swagger

0. Browse to the Web-Chat specification available on Swagger Hub
.center[
> https://app.swaggerhub.com/apis/PIKA-lab/Web-Chat/1.0.0
]

0. Copy and Paste the provided code on [Swagger Editor](http://editor.swagger.io)

0. Navigate the specification using the and try to figure out its meaning

0. Notice that the `message`-related __routes__, and the `ChatMessage` __definition__ are incomplete

0. Take some time to understand the structure of the specification file and the expected functioning of the service
    * _If you cannot understand the functioning of the service **rise your hand now!**_

---

layout: true

### Exercise 6-1: Manipulate a Swagger specification file

---

0. Clone the Lab-6 GitLab repository
.center[
> https://gitlab.com/pika-lab/courses/ds/aa1819/lab-6
]

0. From within the [Swagger Editor](http://editor.swagger.io), try completing the Web-Chat specification

0. Start from the `ChatMessage` __definition__ (instructions on the next slide)

0. Then complete the `message`-related __reoutes__, i.e.
    - `GET /rooms/{chatRoomName}/messages`
    - `POST /rooms/{chatRoomName}/messages`

0. Once done, copy & paste the complete Swagger specification file into the
.center[
> `src/spec/resources/web-chat.yaml`
]

    file within your local repository, then commit & push

---

#### The `ChatMessage` __definition__

A `ChatMessage` is a data structure carrying the following information:

* a link to the `chatRoom` containing the message

<!-- pause -->

* the `index` of the message within the chat room

<!-- pause -->

* the data of the user representing the `sender` of the message

<!-- pause -->

* a `timestamp` of the message, representing the moment when the message was added to the chat room
    - notice that a timestamp is a _string_ adhering to the `date-time` format

<!-- pause -->

* the `content` of the message, i.e. a string of arbitrary length provided by the human user

<!-- pause -->

_Notice that no field is or can be **mandatory**_

---

#### The `GET /rooms/{chatRoomName}/messages` route

- It can be used by the members of the chat room named `{chatRoomName}` to read the (most recent) messages

<!-- pause -->

- It may produce different responses, depending on the role of the requesting client:
    * `admin`s can always retrieve any message from all chat rooms
    * `user`s can do it only if they are members owners
    *  unlogged clients can do it only if the chat room is public

<!-- pause -->

- It may return a response having the following status codes:
    * __`200`__: if everything is fine (in this case a `ListOfMessages` is returned)
    * __`401`__: if the provided credentials are wrong
    * __`403`__: if the authenticated client has no right to read the messages
    * __`404`__: if no chat room named `{chatRoomName}` actually exists

<!-- pause -->

- In case of a `200` response, the result may be encoded using the `xml`, `json`, and `yaml` MIME types

---

#### The `GET /rooms/{chatRoomName}/messages` route

- Similarly to the `GET /rooms` and `GET /users` routes, it supports the following _query parameters_ regulating which and how many messages are returned:
    * __`limit`__: integer, specifies the maximum amount of messages to be returned, defaults to `10`
    * __`skip`__: integer, specifies how many messages should be skipped from the most recent one, defaults to `0`
    * __`filter`__: string, only selects those messages _containing_ the value of this string in their content, defaults to `""` (the empty string)

    _Such parameters represent a common pattern for __bulk GET__ operations: they make it possible to select a "window" on a possible very large sequence of results_

<!-- pause -->

- It supports the `http` protocol

<!-- pause -->

- It supports the `credential`-based security schema, analogously to the other routes

<!-- pause -->

- The name of the corresponding operation on the server side is `readAllUsers`

---

#### The `POST /rooms/{chatRoomName}/messages` route

- It can be used by the members of the chat room named `{chatRoomName}` to publish a new message

<!-- pause -->

- New messages are stored in the request body, as objects of type `ChatMessage`, encoded using the `xml`, `json`, or `yaml` MIME types
    * The `sender` object must only contain the minimal set of fields necessary to identify the sender (i.e. `id`, `username`, or `email`)

<!-- pause -->

- Only members, owners, or `admin`s can publish new messages

<!-- pause -->

- It may return a response having the following status codes:
    * __`200`__: if everything is fine (in this case a `ChatMessage` is returned)
    * __`400`__: if the provided `ChatMessage` object is badly compled (i.e. more fields are defined that what is strictly necessary)
    * __`401`__: if the provided credentials are wrong
    * __`403`__: if the authenticated client has no right to publish messages
    * __`404`__: if no chat room named `{chatRoomName}` actually exists

---

#### The `POST /rooms/{chatRoomName}/messages` route

- In case of a `200` response, the result may be encoded using the `xml`, `json`, or `yaml` MIME types

<!-- pause -->

- It supports the `http` protocol

<!-- pause -->

- It supports the `credential`-based security schema, analogously to the other routes

<!-- pause -->

- The name of the corresponding operation on the server side is `createChatRoomMessage`

---

layout: true

### Exercise 6-2: Implement a ReSTfull web service

---

- Import the cloned repository into your favourite IDE, __as a Gradle project__

<!-- pause -->

- Look at the provided source code within the
.center[
> `src/main/java/`
]

    directory of your local reposotory

- It contains a _partial_ **implementation** of the Web-Chat service defined so far
    - it relies on the [Vert.x library](https://vertx.io/), which enables people to quickly produce lightweight  asynchronous applications, there including (ReSTfull) web services

    - the Vert.x manual for Java is available [here](https://vertx.io/docs/vertx-core/java/)

- The general architecture is described in the following slides. You should try to get a deep understanding of the 
    * `it.unibo.sd1819.lab6.webchat.routes.`__`UsersPath`__
    * `it.unibo.sd1819.lab6.webchat.api.`__`UsersApiImpl`__

    classes, which are provided as example

---

- You must complete the implementation in order to make it totally adherent to the Swagger specification. 
This implies completing the following classes:
    * `it.unibo.sd1819.lab6.webchat.api.`__`RoomsApiImpl`__
    * `it.unibo.sd1819.lab6.webchat.api.`__`MembersApiImpl`__
    * `it.unibo.sd1819.lab6.webchat.api.`__`MessagesApiImpl`__
    * `it.unibo.sd1819.lab6.webchat.routes.`__`MessagesPath`__
    * `it.unibo.sd1819.lab6.webchat.routes.`__`MembersPath`__

    _You should edit the parts containing a comment in the form `\\ TODO: <hint>` or an exception of type `NotImplementedError`_

- Starting the service is as simple as running:
.center[
> `./gradlew run`
]

    within the project root. This will make the `it.unibo.sd1819.lab6.webchat`.__`Service`__ web service start listening on port `8080`

---

- While developing, you can test your implementation by means of the [Postman tool](https://www.getpostman.com/)
    * Alternatively, you can let the Swagger Editor generate `curl` invocations for you

- Your solution must satisfy all tests contained within the `TestMainApiVerticle` class
    * Of course, you cannot alter them

- Commit & Push frequently

---

layout: true

### Exercise 6-2: Architecture

---

#### Object Spaces for Storage

- The web service will need to store the data somewhere

<!-- pause -->

- We provided you the notion of `ObjectSpace<T>`, i.e. a tuple space containing Java objects of type `T`

    ```java
    ObjectSpace<Object> ts = new ObjectSpace<>();

    ts.write(ObjectTuple.of(new Integer(1)));   // Objects must be wrapped within
    ts.write(ObjectTuple.of(new Double(2.0)));  // the ObjectTuple.of(...) method
    ts.write(ObjectTuple.of(new Float(3f)));    // before being written on the TS

    // templates are predicates in the form:
    // ObjectTemplate.anyOfType(<type filter>).where(<condition lambda>)
    CompletableFuture<Optional<ObjectTuple<Object>>> f =  ts.tryTake(
        ObjectTemplate.anyOfType(Number.class).where(n -> n.doubleValue() >= 1.5)
    );

    f.thenAcceptAsync(optN -> {
        if (optN.isPresent()) {
            System.out.println(optN.get().get()); // Either 2.0 or 3.0
        } else {
            System.err.println("This should never happen");
        }
    });
    ```

---

#### Predefined Object Spaces for the Web Chat app

The `it.unibo.sd1819.lab6.webchat.storage.`__`ObjectSpaces`__ class comes with some predefined object spaces you can use for your exercise, by means of the following _static_ methods:

<!-- pause -->

* the `ObjectSpace<User> users()` method always returns the object space aimed at containing `User`s

<!-- pause -->

* the `ObjectSpace<ChatMessage> chatRoomMessages(String chatRoomName)` method always returns the object space aimed at containing `ChatMessage`s for the chat room named `chatRoomName`

<!-- pause -->

* the `ObjectSpace<User> chatRoomMembers(String chatRoomName)` method always returns the object space aimed at containing the members of the chat room named `chatRoomName`

<!-- pause -->

* the `ObjectSpace<ChatRoom> chatRooms()` method always returns the object space aimed at containing the metadata of all `ChatRoom`s

---

#### The presentation layer

The `it.unibo.sd1819.lab6.webchat.`__`presentation`__ package is aimed at containing the classes corresponding to the data type __definitions__ from the Swagger specification. 
They all are subclasses of `Representation`, and they constitute the __presentation layer__ of our application:

- `it.unibo.sd1819.lab6.webchat.presentation.`__`User`__
- `it.unibo.sd1819.lab6.webchat.presentation.`__`Link`__
- `it.unibo.sd1819.lab6.webchat.presentation.`__`ChatRoom`__
- `it.unibo.sd1819.lab6.webchat.presentation.`__`ChatMessage`__
- `it.unibo.sd1819.lab6.webchat.presentation.`__`ListOfUsers`__
- `it.unibo.sd1819.lab6.webchat.presentation.`__`ListOfChatRooms`__
- `it.unibo.sd1819.lab6.webchat.presentation.`__`ListOfMessages`__

<!-- pause -->

Such classes share some peculiarities:

<!-- pause -->

- they are fluent:
```java
new User(null, "gciatto", null, "givanni.ciatto@unibo.it", null, null);
// is equivalent to
new User().setUsername("gciatto").setEmail("givanni.ciatto@unibo.it");
```

---

#### The presentation layer

- they all have a constructor producing a __deep copy__ of an object
```java
User u1 = // ...
User u2 = new User(u1); // deep copy of u1
```

<!-- pause -->

- they all inherit the following _instance_ methods:

    * __`toJSONString()`__, creating a `JSON` representation of the resource
    * __`toYAMLString()`__, creating a `YAML` representation of the resource
    * __`toXMLString()`__, creating a `XML` representation of the resource
    * __`toMIMETypeString(String mimeType)`__, calling the proper conversion method, depending on the provided `MIME`
 type

<!-- pause -->

- they all expose the following _static_ methods:

    * __`fromJSON(String payload)`__, parsing a resource from a `JSON` string
    * __`fromYAML(String payload)`__, parsing a resource from a `YAML` string
    * __`fromXML(String payload)`__, parsing a resource from a `XML` string
    * __`parse(String mimeType, String payload)`__, parsing a resource from a string adhering to the `MIME` type provided as argument

---

#### The `it.unibo.sd1819.lab6.webchat.exceptions.`__`HttpError`__ class hierarchy

__`HttpError`__: base abstract exception for HTTP errors

* __`ClientError`__: base abstract exception for 4xx errors

    - __`BadContentError`__: provokes a 400 status code as response

    - __`UnauthorizedError`__: provokes a 401 status code as response

    - __`ForbiddenError`__: provokes a 403 status code as response

    - __`NotFoundError`__: provokes a 404 status code as response

    - __`ConflictError`__: provokes a 409 status code as response

* __`ServerError`__: base abstract exception for 5xx errors

    - __`InternalServerError`__: provokes a 500 status code as response
        + it is the common wrapper for unexpected/uncaught exceptions

    - __`NotImplementedError`__: provokes a 501 status code as response

---

#### The `it.unibo.sd1819.lab6.webchat.routes.`__`Path`__ class hierarchy

__`WebChatPath`__: handles the path `/web-chat/v1/*`, delegating sub paths to:

* __`UsersPath`__: handles the path `/web-chat/v1/users/*`

* __`RoomsPath`__: handles the path `/web-chat/v1/rooms/*`, delegating sub paths to:

    - __`MessagesPath`__: handles the path `/web-chat/v1/rooms/{chatRoomName}/messages/*`

    - __`MembersPath`__: handles the path `/web-chat/v1/rooms/{chatRoomName}/members/*`

---

#### The `it.unibo.sd1819.lab6.webchat.routes.`__`Path`__ class hierarchy

Each subclass of `Path` must configure its own __routes__. 
For instance:
```java
public class UsersPath extends Path {
    public UsersPath() { super("/users"); }

	@Override protected void setupRoutes() {
        addRoute(HttpMethod.POST, this::post) // the method `post` will 
            .consumes("application/json")     // handle this route
            .produces("application/json");    // it supports the JSON type

        addRoute(HttpMethod.GET, this::get)
            .produces("application/json");

        addRoute(HttpMethod.GET, "/:identifier", this::getUser)
            .produces("application/json");

        addRoute(HttpMethod.PUT, "/:identifier", this::putUser)
            .consumes("application/json")
            .produces("application/json");
    }
    
    private void post(RoutingContext routingContext) { 
        // handles the POST method on `/user`
    }
}   
```

---

#### The structure of a route handler method (input)

```java
private void post(RoutingContext rc) {
    UsersApi api = UsersApi.get(rc);
    //             ↑ this is where the actual business logic is implemented

    Future<Link> result = Future.future(); // this is a Vert.x future!
    result.setHandler(responseHandler(rc));
    //                ↑ this is where the result is marshalled

    try {
        //               ↓ this is where params are unmarshalled
        User user = User.parse(rc.parsedHeaders().contentType().value(), 
                               rc.getBodyAsString());

        validateUserForPost(user); // validating input params
        //  ↓ actually execute the business logic
        api.createUser(user, result.completer()); 
    } catch(HttpError e) { // if an HTTP error occurs
        result.fail(e); // its status code is sent back to the client
    } catch (IOException | IllegalArgumentException e) { // if User.parse fails
        result.fail(new BadContentError(e)); // 400 status code is returned
    }
}
```

---

#### The structure of a route handler method (output)

```java
private void get(RoutingContext rc) {
  UsersApi api = UsersApi.get(rc);
  //             ↑ this is where the actual business logic is implemented

  Future<ListOfUsers> result = Future.future(); // this is a Vert.x future!
  result.setHandler(responseHandler(rc, this::cleanUsers));
  // this is where the result is cleaned & marshalled  ↑ (e.g. remove passwords)

  try { // here we try to parse the query parameter ...
      Integer skip = Optional.ofNullable(rc.queryParams().get("skip"))
        .map(Integer::parseInt).orElse(0); // ... providing defaults ...
      Integer limit = Optional.ofNullable(rc.queryParams().get("limit"))
        .map(Integer::parseInt).orElse(10); // ... in case of missing values
      String filter = Optional.ofNullable(rc.queryParams().get("filter"))
        .orElse("");

      //  ↓ actually execute the business logic
      api.readAllUsers(skip, limit, filter, result.completer());
  } catch(HttpError e) { // if an HTTP error occurs
      result.fail(e); // its status code is sent back to the client
  } catch (IllegalArgumentException e) { // if Integer.parseInt fails
      result.fail(new BadContentError(e)); // 400 status code is returned
  }
}
```

---

#### The `it.unibo.sd1819.lab6.webchat.api.`__`AbstractApi`__ class hierarchy


The `AbstractApi` class is the base class for all the `*Api` classes actually implementing the business logic of our web service.
It provides some useful _protected_ methods which can be employed in your exercises:

<!-- pause -->

- __`Optional<User> getAuthenticatedUser()`__: if the requesting client provided some credentials, and such credentials actually correspond to some registered user, the corresponding `User` data is returned, otherwhise an empty `Optional` is returned. In case of wrong credentials, an __`UnauthorizedError`__ is thrown

<!-- pause -->

- __`boolean isAuthenticatedUserAtLeast(User.Role role)`__: this method simply checks if the authenticated user has at least the `role` rights, returning `false` in case he/she doesn't

<!-- pause -->

- __`void ensureAuthenticatedUserAtLeast(User.Role role)`__: this method simply checks if the authenticated user has at least the `role` rights, throwing a __`ForbiddenError`__ in case he/she doesn't

---

#### Example: the `it.....api.`__`UserApiImpl::readAllUsers`__ method

```java
public void readAllUsers(Integer skip, Integer limit, String filter, 
                         Handler<AsyncResult<ListOfUsers>> handler) {
  // no need to check arguments: the route handler cleaned them

  ensureAuthenticatedUserAtLeast(User.Role.ADMIN);
  // only admins can go on 

  users.readAll(
    ObjectTemplate.anyOfType(User.class)
      .where(it -> it.getUsername().contains(filter))
  ).thenAcceptAsync(tuples -> // asynchronously handle the tuples
    handler.handle( // call handler.handle(...) to serve the request
      Future.succeededFuture( // create an already succeeded future
        new ListOfUsers( // creates the requested list of users...
            tuples.stream()
              .map(ObjectTuple::get)
              .skip(skip)    // ... skipping and limiting the results
              .limit(limit)
        )
      )
    )
  );

  // NOTICE that there is no need to remove passwords from the users...
}
```

---

layout: false
class: center, middle

# T-6 -- All we need is ReST

.middle.center[[__Giovanni Ciatto__](mailto:giovanni.ciatto@unibo.it)]

.middle.center[and]

.middle.center[[Andrea Omicini](mailto:andrea.omicini@unibo.it)]